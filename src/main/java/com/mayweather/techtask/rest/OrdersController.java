package com.mayweather.techtask.rest;

import com.mayweather.techtask.exceptions.NoObjectException;
import com.mayweather.techtask.models.PageableApi;
import com.mayweather.techtask.models.Status;
import com.mayweather.techtask.models.domain.OrderEntity;
import com.mayweather.techtask.models.vm.OrderVM;
import com.mayweather.techtask.services.OrderService;
import com.mayweather.techtask.utils.changers.order.OrderChanger;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.mayweather.techtask.GeneralConstants.ID;
import static com.mayweather.techtask.GeneralConstants.ID_PATH;
import static com.mayweather.techtask.utils.changers.order.OrderChanger.toOrderEntity;
import static com.mayweather.techtask.utils.changers.order.OrderChanger.toOrderVM;
import static org.springframework.http.HttpStatus.*;

@Api("Order API")
@RestController("/orders")
@AllArgsConstructor
@Slf4j
public class OrdersController {

    private final OrderService orderService;

    @ApiOperation(value = "Create an order")
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderVM> create(@Valid @RequestBody final OrderVM orderVM, BindingResult bindingResult) {
        BindingErrorsResponse errorsResponse = new BindingErrorsResponse();
        HttpHeaders httpHeaders = new HttpHeaders();
        if (bindingResult.hasErrors() || (orderVM == null)) {
            errorsResponse.addAllErrors(bindingResult);
            httpHeaders.add("errors", errorsResponse.toJSON());
            return new ResponseEntity<>(orderVM, httpHeaders, BAD_REQUEST);
        }
        final OrderEntity entity = toOrderEntity(orderVM);
        entity.setStatus(Status.NEW);
        final OrderEntity saved = orderService.save(entity);
        final OrderVM vm = toOrderVM(saved);
        return new ResponseEntity<>(vm, CREATED);
    }

    @ApiOperation(value = "Get an order by ID")
    @GetMapping(value = ID_PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OrderVM> getById(@PathVariable(ID) final Long id) throws NoObjectException {
        final OrderEntity orderEntity = orderService.findById(id);
        if (orderEntity == null) {
            throw new NoObjectException();
        }
        final OrderVM orderVM = toOrderVM(orderEntity);
        return new ResponseEntity<>(orderVM, OK);
    }

    @ApiOperation(value = "Delete an order by ID")
    @DeleteMapping(value = ID_PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteById(@PathVariable(ID) final Long id) throws NoObjectException {
        final OrderEntity orderEntity = orderService.findById(id);
        if (orderEntity == null) {
            throw new NoObjectException();
        }
        final OrderVM orderVM = toOrderVM(orderEntity);
        orderService.delete(orderVM.getId());
        return new ResponseEntity(OK);

    }

    @ApiOperation(value = "Find all orders")
    @PageableApi
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<OrderVM>> getPerPage(final Pageable pageable) throws NoObjectException {
        final Page<OrderEntity> orders = orderService.getPerPage(pageable);
        final List<OrderVM> orderVMS = orders.getContent().stream().map(OrderChanger::toOrderVM).collect(Collectors.toList());
        if (orderVMS.isEmpty()) {
            throw new NoObjectException();
        }
        return new ResponseEntity<>(orderVMS, OK);
    }

    @ApiOperation("Confirm an order")
    @PatchMapping(value = (ID_PATH + "/confirm"), produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> confirmOrder(@PathVariable Long id) throws NoObjectException {
        final OrderEntity orderEntity = orderService.findById(id);
        if (orderEntity == null) {
            throw new NoObjectException();
        }
        final OrderVM orderVM = toOrderVM(orderEntity);
        final boolean confirm = orderService.confirm(orderVM);
        return new ResponseEntity<>(confirm, OK);
    }


    //i need this?
  /* @PutMapping(ID_PATH)
    public ResponseEntity update(@PathVariable(ID) final Long id, @RequestBody final OrderVM orderVM) {
        final OrderEntity entity = toOrderEntity(orderVM);
        entity.setId(id);
        final OrderEntity orderEntity = orderService.save(entity);
        final OrderVM vm = toOrderVM(orderEntity);
        return new ResponseEntity<>(vm, HttpStatus.OK);
    }
    */
}
