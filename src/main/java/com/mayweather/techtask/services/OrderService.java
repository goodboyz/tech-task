package com.mayweather.techtask.services;


import com.mayweather.techtask.models.domain.OrderEntity;
import com.mayweather.techtask.models.vm.OrderVM;

public interface OrderService extends CrudService<OrderEntity> {
    boolean confirm(OrderVM orderVM);
}
